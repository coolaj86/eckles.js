// Copyright 2016-2018 AJ ONeal. All rights reserved
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict';

var curves = {
  "P-256": "prime256v1" // 65
, "secp256r1": "prime256v1"
, "P-384": "secp384r1" // 97
, "P-521": "secp521r1" // 133
};
var crv = "P-256";
var ecdh = require('crypto').createECDH(curves[crv] || crv);
var keys = ecdh.generateKeys(null, 'uncompressed');
console.log(keys.length);
console.log(keys.toString('hex'));
keys = keys.slice(1);
var x = keys.slice(0, keys.byteLength / 2);
var y = keys.slice(keys.byteLength / 2);
while (0 === x[0]) { x = x.slice(1); }
while (0 === y[0]) { y = y.slice(1); }
console.log({
  kty: "EC"
, crv: "P-XXX"
, x: _toUrlBase64(x)
, y: _toUrlBase64(y)
});

function _toUrlBase64(buf) {
  return buf.toString('base64')
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=/g,"")
  ;
}
