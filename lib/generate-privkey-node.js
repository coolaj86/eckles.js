// Copyright 2016-2018 AJ ONeal. All rights reserved
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict';

module.exports = function (crv) {
  var keypair = require('crypto').generateKeyPairSync(
    'ec'
  , { namedCurve: crv
    , privateKeyEncoding: { type: 'sec1', format: 'pem' }
    , publicKeyEncoding: { type: 'spki', format: 'pem' }
    }
  );
  var result = { privateKeyPem: keypair.privateKey.trim() };
  return result;
};

if (require.main === module) {
  var keypair = module.exports('P-256');
  console.info(keypair.privateKeyPem);
}
